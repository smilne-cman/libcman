#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <libfiles/libfiles.h>
#include <libini/libini.h>
#include "CacheService.h"
#include "SettingsService.h"

#include "labels.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static const char *get_cache_path(const char *filename, const char *extension);
static IniFile *get_metadata(char *name);
static IniFile *new_metadata(char *name);
static char *get_filename(char *name, char *version);
static void clean_library(char *name);
static void copy_file(char *sourceDir, char *sourceFile, char *targetDir, char *targetFile);
static void extract_file();
static char *get_latest(char *name);
static char* replace_char(char* str, char find, char replace);

/* Global Variables ***********************************************************/
static CacheService *instance = NULL;

/* Functions ******************************************************************/
CacheService *get_cache_service() {
  if (instance != NULL) return instance;

  SettingsService *settings = get_settings_service();
  const char *cache_dir = settings->get("cache-dir");
  if (!directory_exists(cache_dir)) {
    // FIXME -- This doesn't create the cache if ~/.cman directory doesn't exist
    puts(LABEL_INITIALISING_CACHE);
    directory_new(cache_dir);
  }

  CacheService *service = (CacheService *)malloc(sizeof(CacheService));

  service->exists = cache_exists;
  service->version_exists = cache_version_exists;
  service->get = cache_get;
  service->put = cache_put;
  service->get_latest = get_latest;

  return service;
}

void set_cache_service(CacheService *service) {
  instance = service;
}

int cache_exists(char *name) {
  IniFile *metadata = get_metadata(name);

  return metadata != NULL;
}

int cache_version_exists(char *name, char *version) {
  return get_filename(name, version) != NULL;
}

void cache_get(char *name, char *version, char *path) {
  char *rootDirectory = (char *)malloc(sizeof(char) * 1024);
  rootDirectory = getcwd(rootDirectory, 1024);

  SettingsService *settings = get_settings_service();

  chdir(path);
  clean_library(name);
  copy_file(settings->get("cache-dir"), get_filename(name, version), name, "");

  chdir(name);
  extract_file();
  chdir(rootDirectory);

  free(rootDirectory);
}

void cache_put(char *name, char *version, char *author, char *path, char *filename) {
  IniFile *metadata = get_metadata(name);
  SettingsService *settings = get_settings_service();

  if (metadata == NULL) {
    metadata = new_metadata(name);
  }

  if (ini_get(metadata, NULL, "name") == NULL) {
    ini_put(metadata, NULL, "name", name);
  }

  if (author != NULL) {
    ini_put(metadata, NULL, "author", author);
  }

  ini_put(metadata, NULL, "latest", version);

  ini_put(metadata, version, "archive", filename);
  ini_put(metadata, version, "checksum", "00000000000000000000000000000000");
  ini_put(metadata, version, "released", "YYYY-MM-DD");
  ini_put(metadata, version, "deprecated", "false");

  ini_write(metadata);
  copy_file(path, filename, settings->get("cache-dir"), filename);
}

static IniFile *get_metadata(char *name) {
  char *filename = get_cache_path(name, ".meta.ini");
  if (!file_exists(filename)) {
    return NULL;
  }

  IniFile *metadata = ini_open(filename);
  ini_read(metadata);

  return metadata;
}

static IniFile *new_metadata(char *name) {
  char *filename = get_cache_path(name, ".meta.ini");
  IniFile *metadata = ini_open(filename);

  ini_put(metadata, NULL, "name", name);
  ini_put(metadata, NULL, "latest", "");

  return metadata;
}

static const char *get_cache_path(const char *filename, const char *extension) {
  const char *fullname = (const char *)malloc(sizeof(char) * 1024);

  sprintf(fullname, "%s/%s%s", get_settings_service()->get("cache-dir"), filename, extension);

  return fullname;
}

static char *get_filename(char *name, char *version) {
  IniFile *metadata = get_metadata(name);

  char *filename = ini_get(metadata, version == NULL ? get_latest(name) : version, "archive");

  return filename;
}

static void clean_library(char *name) {
  if (!directory_exists(name)) {
    directory_new(name);
  } else {
    directory_remove(name);
    directory_new(name);
  }
}

static void copy_file(char *sourceDir, char *sourceFile, char *targetDir, char *targetFile) {
  const char *buffer = (const char *)malloc(sizeof(char) * 1024);

  sprintf(buffer, "cp %s/%s %s/%s", sourceDir, sourceFile, targetDir, targetFile);
  system(buffer);

  free(buffer);
}

static void extract_file() {
  system("gunzip *.gz");
  system("tar -xf *.tar");
  system("rm *.tar");
}

static char *get_latest(char *name) {
  return ini_get(get_metadata(name), NULL, "latest");
}

// TODO -- Add to libstring
static char* replace_char(char* str, char find, char replace) {
  char *current_pos = strchr(str,find);
  while (current_pos){
    *current_pos = replace;
    current_pos = strchr(current_pos,find);
  }
  return str;
}
