#ifndef __labels__
#define __labels__

/* Includes *******************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/

extern const char LABEL_INITIALISING_CACHE[];
extern const char LABEL_SETTING_NOT_FOUND[];

#endif
