#include <stdio.h>
#include <stdlib.h>
#include <pwd.h>
#include <libini/libini.h>
#include <libcollection/set.h>
#include <libprompt/libprompt.h>
#include "SettingsService.h"
#include "labels.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static IniFile *settings_load();
static const char *settings_default(const char *name);
static const char *get_home_dir();
static const char *get_path();
static const char *get_cache_dir();

/* Global Variables ***********************************************************/
const char *SETTINGS_FILENAME = ".cmanrc";
const char *CACHE_DIRECTORY = ".cman/cache";
const char *DEFAULT_INSTALL_DIR = "/usr/local/bin";

const char *SETTING_CACHE_DIR = "cache-dir";
const char *SETTING_TEST_ARGS = "test-arguments";
const char *SETTING_INSTALL_DIR = "install-dir";

static SettingsService *instance = NULL;
static Set *setting_names = NULL;
static IniFile *cache = NULL;

/* Functions ******************************************************************/
SettingsService *get_settings_service() {
  if (instance != NULL) return instance;

  instance = (SettingsService *)malloc(sizeof(SettingsService));

  instance->get = settings_get;
  instance->set = settings_set;
  instance->save = settings_save;
  instance->list = settings_list;

  return instance;
}

void set_settings_service(SettingsService *service) {
  instance = service;
}

const char *settings_get(const char *name) {
  if (!set_contains(settings_list(), name)) {
    error(LABEL_SETTING_NOT_FOUND);
    return NULL;
  }

  const char *value = ini_get(settings_load(), NULL, name);
  if (value == NULL) {
    return settings_default(name);
  }

  return value;
}

void settings_set(const char *name, const char *value) {
  if (!set_contains(settings_list(), name)) {
    error(LABEL_SETTING_NOT_FOUND);
    return;
  }

  ini_put(settings_load(), NULL, name, value);
}

void settings_save() {
  ini_write(settings_load());
}

Set *settings_list() {
  if (setting_names != NULL) return setting_names;

  setting_names = set_news();

  set_adds(setting_names, SETTING_CACHE_DIR);
  set_adds(setting_names, SETTING_TEST_ARGS);
  set_adds(setting_names, SETTING_INSTALL_DIR);

  return setting_names;
}

static IniFile *settings_load() {
  if (cache != NULL) return cache;

  cache = ini_open(get_path());

  ini_read(cache);
  return cache;
}

static const char *settings_default(const char *name) {
  if (!strcmp(name, SETTING_CACHE_DIR)) {
    return get_cache_dir();
  } else if (!strcmp(name, SETTING_TEST_ARGS)) {
    return "";
  } else if (!strcmp(name, SETTING_INSTALL_DIR)) {
    return DEFAULT_INSTALL_DIR;
  }

  return NULL;
}

static const char *get_home_dir() {
  struct passwd *pw = getpwuid(getuid());

  return pw->pw_dir;
}

static const char *get_path() {
  const char *path = (const char *)malloc(sizeof(char) * 1024);

  sprintf(path, "%s/%s", get_home_dir(), SETTINGS_FILENAME);

  return path;
}

static const char *get_cache_dir() {
  const char *path = (const char *)malloc(sizeof(char) * 1024);

  sprintf(path, "%s/%s", get_home_dir(), CACHE_DIRECTORY);

  return path;
}
