#include <stdio.h>
#include <stdlib.h>
#include <libstring/libstring.h>
#include <libfiles/libfiles.h>
#include <libcollection/types.h>
#include <libcollection/set.h>
#include <libcollection/map.h>
#include <libcollection/compare.h>
#include <libcollection/iterator.h>
#include <libsemver/libsemver.h>
#include "ProjectService.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void setup_paths(Project *project);
static void setup_compiler(Project *project);
static const char *def(const char *value, const char *defaultValue);
static Project *parse_project(char *filename, int isLibrary);
static void setup_libraries(Project *project);
static void setup_scripts(Project *project);
static void setup_extensions(Project *project);
static int query_starts_with(MapEntry *value, char *arg);
static int library_compare(void *value, void *target);
static ProjectType parseLibraryType(int isLibrary, const char *type);

/* Global Variables ***********************************************************/
static ProjectService *instance = NULL;
static Project *project_cache = NULL;

/* Functions ******************************************************************/
ProjectService *get_project_service() {
  if (instance != NULL) return instance;

  ProjectService *service = (ProjectService *)malloc(sizeof(ProjectService));

  service->current = project_current;
  service->load = project_load;
  service->add = project_add;
  service->remove = project_remove;
  service->installed = project_is_installed;

  return service;
}

void set_project_service(ProjectService *service) {
  instance = service;
}

Library *library_new(const char *name, const char *version) {
  Library *library = (Library *)malloc(sizeof(Library));

  library->name = name;
  library->version = version == NULL ? "*.*.*" : version;

  return library;
}

Project *project_current() {
  if(project_cache != NULL) return project_cache;

  Project *project = NULL;

  if(file_exists("project.ini")) {
    project = parse_project("project.ini", FALSE);
  } else if(file_exists("library.ini")) {
    project = parse_project("library.ini", TRUE);
  } else {
    return NULL;
  }

  project_cache = project;
  return project;
}

Project *project_load(char *name) {
  Project *project = project_current();
  char *filename = (char *)malloc(sizeof(char) * 1024);

  sprintf(filename, "%s/%s/library.ini", project->paths->library, name);
  if (!file_exists(filename)) {
    return NULL;
  }

  return parse_project(filename, TRUE);
}

void project_add(char *name, char *version) {
  Project *project = project_current();

  ini_put(project->configuration, "library", name, version);
  ini_write(project->configuration);
}

void project_remove(char *name) {
  Project *project = project_current();

  ini_remove(project->configuration, "library", name);
  ini_write(project->configuration);
}

int project_is_installed(Library *library) {
  Project *manifest = project_load(library->name);
  if (manifest == NULL) return FALSE;

  Semver *required = semver_from_string(library->version);
  Semver *available = semver_from_string(manifest->version);

  return semver_compare(required, available) != GREATER;
}

static void setup_paths(Project *project) {
  ProjectPaths *paths = (ProjectPaths *)malloc(sizeof(ProjectPaths));
  IniFile *conf = project->configuration;

  paths->distribution = def(ini_get(conf, "directories", "distribution"), "dist");
  paths->resources = def(ini_get(conf, "directories", "resources"), "res");
  paths->tests = def(ini_get(conf, "directories", "tests"), "test");
  paths->source = def(ini_get(conf, "directories", "source"), "src");
  paths->binary = def(ini_get(conf, "directories", "binary"), "bin");
  paths->library = def(ini_get(conf, "directories", "library"), "lib");
  paths->temporary = def(ini_get(conf, "directories", "temporary"), "tmp");
  paths->documentation = def(ini_get(conf, "directories", "documentation"), "doc");

  project->paths = paths;
}

static void setup_compiler(Project *project) {
  CompilerArguments *compiler = (CompilerArguments *)malloc(sizeof(CompilerArguments));
  IniFile *conf = project->configuration;

  compiler->flags = def(ini_get(conf, "compiler", "flags"), "");

  project->compiler = compiler;
}

static const char *def(const char *value, const char *defaultValue) {
  if(value == NULL) return defaultValue;

  return value;
}

static Project *parse_project(char *filename, int isLibrary) {
  Project *project = (Project *)malloc(sizeof(Project));
  project->configuration = ini_open(filename);

  ini_read(project->configuration);
  project->name = ini_get(project->configuration, NULL, "name");
  project->version = ini_get(project->configuration, NULL, "version");
  project->author = ini_get(project->configuration, NULL, "author");
  project->isLibrary = isLibrary;
  project->type = parseLibraryType(isLibrary, ini_get(project->configuration, NULL, "type"));

  setup_paths(project);
  setup_compiler(project);
  setup_libraries(project);
  setup_scripts(project);
  setup_extensions(project);

  return project;
}

static ProjectType parseLibraryType(int isLibrary, const char *type) {
  if (type == NULL) {
    return isLibrary ? StaticLibrary : StandardProject;
  }

  if (!strcmp(type, "static")) {
    return StaticLibrary;
  } else if (!strcmp(type, "dynamic")) {
    return DynamicLibrary;
  } else if (!strcmp(type, "project")) {
    return StandardProject;
  }
}

static void setup_libraries(Project *project) {
  Set *libraries = set_new(library_compare);
  MapEntry *libs = map_get(project->configuration->entries, "library");

  if (libs == NULL) {
    return;
  }

  Iterator *iterator = map_iterator(libs);
  while(has_next(iterator)) {
    MapEntry *entry = next_map(iterator);

    Library *library = library_new(entry->key, entry->value);

    set_add(libraries, library);
  }

  project->libraries = libraries;
  iterator_destroy(iterator);
}

static void setup_scripts(Project *project) {
  Map *scripts = map_get(project->configuration->entries, "script");

  project->scripts = scripts == NULL ? map_new(map_by_string(string_compare)) : scripts;
}

static void setup_extensions(Project *project) {
  project->extensions = ini_section(project->configuration, "extensions");
}

static int query_starts_with(MapEntry *value, char *arg) {
  return starts_with(arg, value->key);
}

static int library_compare(void *value, void *target) {
  if (value == NULL || target == NULL) {
    return LESS;
  }

  Library *libValue = (Library *)value;
  Library *libTarget = (Library *)target;

  return string_compare(libValue->name, libTarget->name);
}
