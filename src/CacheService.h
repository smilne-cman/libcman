#ifndef __cache__
#define __cache__

/* Includes *******************************************************************/

/* Types **********************************************************************/
typedef struct CacheService {
  int (*exists)(char *name);
  int (*version_exists)(char *name, char *version);
  void (*get)(char *name, char *version, char *path);
  void (*put)(char *name, char *version, char *author, char *path, char *filename);
  char *(*get_latest)(char *name);
} CacheService;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
CacheService *get_cache_service();
void set_cache_service(CacheService *service);
int cache_exists(char *name);
int cache_version_exists(char *name, char *version);
void cache_get(char *name, char *version, char *path);
void cache_put(char *name, char *version, char *author, char *path, char *filename);

#endif
