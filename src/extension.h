#ifndef __extension__
#define __extension__

/* Includes *******************************************************************/
#include <libcommand/libcommand.h>
#include "SettingsService.h"
#include "ProjectService.h"

/* Types **********************************************************************/
typedef struct ExtensionHost {
  void (*register_command)(CommandHandler *handler);  // Registers a command handler to be injected
  SettingsService *(*settings)();                     // Retreives the settings service
  ProjectService *(*project)();                       // Retreives the project servicce
  void (*log)(const char *message);                   // Logs a message to the build log
} ExtensionHost;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/

#endif
