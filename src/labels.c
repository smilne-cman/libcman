#include <stdio.h>
#include <stdlib.h>
#include "labels.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/

/* Global Variables ***********************************************************/
const char LABEL_INITIALISING_CACHE[] = "Initialising local cache...";
const char LABEL_SETTING_NOT_FOUND[] = "Setting not found!";

/* Functions ******************************************************************/
